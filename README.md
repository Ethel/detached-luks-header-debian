# Objectifs :

Cette démarche a pour objectif de créer un système debian chiffré sur un disque et son header sur un media amovible de sorte qu'aucune information sur le type des données ne soit présente sur le disque système.
Prévoir une série de petites modifications pour sécuriser et préparer le systême.
Les opérations sont plus ou moins automatisées dans des scripts.
Leur contenu pourra être détaillé dans la section wiki.

- Etre suffisament documenté pour être réalisable par des personnes sans connaissances techniques
- Les scripts fonctionnent depuis une Tails

# Prérequis :

- Etre connécté à internet
- Pouvoir exécuter des commandes `sudo`. Sur Tails, il fait pour cela définir un mot de passe Administrateur au démarrage.
- Avoir un disque dur dans votre machine pour recevoir la debian (toute donnée sera irrémédiablement éffacée)L
- Avoir une clé USB pour recevoir le header de la partition luks et le bootloader de debian. Elle peut être toute petite, moins de 20Go seront utilisés. Prenez là de bonne qualité, faites en des backups, si vous la perdez, votre système ne sera pas récupérable.

# Hints :

- Depuis votre système :
```console
sudo ./install.sh
```

# Le repertoire contient :

- Un script `install.sh` 

- Un dossier `scripts` contenant les scripts de configuration du nouveau système et d'installation de paquets utiles.
- Un dossier `data/dynamic` comportant les logiciels téléchargés pendant l'installation.
- Un dossier `data/static` comportant les fichiers de configuration qui utilisés pendant l'installation, triés en fonction de s'il s'agit de configuration du système ou d'installation de paquets.
- Un dossier `data/static/ressources` contenant les sources de ce tutoriel.
- Un dossier `documents` dans lequel vous pouvez copier des fichiers qui seront placés dans le repertoire `~/Documents` du nouveau système.

# Procédure :

- Télécharger le dossier (petite icone à gauche de `clone` en haut de cette page) et le dézipper. 
- Entrer dans le dossier et y ouvrir un terminal (clic droit, ouvrir dans un terminal).
- Connecter votre clé USB
- Executer le script `install.sh` et répondre à toutes ses questions. Le sript vous proposera notamment d'effacer le disque dur et la clé usb, ces opérations peuvent être très longues, munissez vous de patience).
- Redémarrer sur le nouveau système avec votre clé branchée. 
