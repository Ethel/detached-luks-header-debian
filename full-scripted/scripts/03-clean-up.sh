#!/bin/bash 

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

### Recuperation des variables
MACHINE=$(cat data/dynamic/variables/MACHINE)
USERNAME=$(cat data/dynamic/variables/USERNAME)
DEVBOOT=$(cat  data/dynamic/variables/DEVBOOT)
DEVSYST=$(cat data/dynamic/variables/DEVSYST) 
ROOTUUID=$(cat data/dynamic/variables/ROOTUUID) 
SWAPUUID=$(cat data/dynamic/variables/SWAPUUID) 
EFIUUID=$(cat data/dynamic/variables/EFIUUID) 
BOOTUUID=$(cat data/dynamic/variables/BOOTUUID) 
HEADERUUID=$(cat data/dynamic/variables/HEADERUUID) 

# Exit on error
# set -e 

# Close installation and clean up

umount data/dynamic/luks/luks-header
rm -rf data/dynamic/luks/luks-header
umount target/dev/pts
umount target/sys
umount target/proc
umount target/mnt/tmp
umount target/boot/efi
umount target/boot
umount target/dev
swapoff /dev/mapper/${MACHINE}----vg-swap
umount target/
lvchange -an /dev/${MACHINE}--vg/swap
lvchange -an /dev/${MACHINE}--vg/root
cryptsetup luksClose ${DEVSYST}_decrypt
rm -rvf target

# [[ $? -gt 0 ]] && echo -e "${RED}Démontage des volumes échoué. Exit.${STD}" && exit 1
rm -vrf data/dynamic/variables/*
rm -vrf data/dynamic/packages/*
echo -e "${RED}L'installation est finie, vous pouvez redémarrer sur votre nouveau système.${STD}"
