#!/bin/bash

# Exit on error
set -e 

# Variables de mise en page

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

### Recuperation des variables
MACHINE=$(cat /mnt/tmp/install/data/variables/MACHINE)
USERNAME=$(cat /mnt/tmp/install/data/variables/USERNAME)
DEVBOOT=$(cat  /mnt/tmp/install/data/variables/DEVBOOT)
DEVSYST=$(cat /mnt/tmp/install/data/variables/DEVSYST) 
ROOTUUID=$(cat /mnt/tmp/install/data/variables/ROOTUUID) 
SWAPUUID=$(cat /mnt/tmp/install/data/variables/SWAPUUID) 
EFIUUID=$(cat /mnt/tmp/install/data/variables/EFIUUID) 
BOOTUUID=$(cat /mnt/tmp/install/data/variables/BOOTUUID) 
HEADERUUID=$(cat /mnt/tmp/install/data/variables/HEADERUUID) 

#

### Configure Macchanger

# Configuration de l'interface réseau

# echo -e "${BOLD}Configuration de l'anonymisation de l'adresse MAC.${STD}"

# Spoof current mac adresses
# ifconfig wlan0 down
# ifconfig eth0 down
# macchanger -r wlan0
# macchanger -r eth0

# Enable macchanger at startup
# cp /mnt/tmp/install/config/macchanger/changemac@.service /etc/systemd/system/
# chown root:root /etc/systemd/system/changemac@.service


# Configure SSH if config exists

if [ -f "/mnt/tmp/documents/ssh/config" ]; 
	then
	mkdir -pv /home/${USERNAME}/.ssh
	chmod  700 /home/${USERNAME}/.ssh
	cp -vrf /mnt/tmp/documents/ssh/config >> /home/${USERNAME}/.ssh/
	chmod 600 /home/${USERNAME}/.ssh/config
	chown -R ${USERNAME}:{$USERNAME} /home/{$USERNAME}/.ssh
fi

