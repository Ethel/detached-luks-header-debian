#!/bin/bash

# Exit on error
# set -e 

### Recuperation des variables
MACHINE=$(cat /mnt/tmp/install/data/variables/MACHINE)
USERNAME=$(cat /mnt/tmp/install/data/variables/USERNAME)
DEVBOOT=$(cat  /mnt/tmp/install/data/variables/DEVBOOT)
DEVSYST=$(cat /mnt/tmp/install/data/variables/DEVSYST) 
ROOTUUID=$(cat /mnt/tmp/install/data/variables/ROOTUUID) 
SWAPUUID=$(cat /mnt/tmp/install/data/variables/SWAPUUID) 
EFIUUID=$(cat /mnt/tmp/install/data/variables/EFIUUID) 
BOOTUUID=$(cat /mnt/tmp/install/data/variables/BOOTUUID) 
HEADERUUID=$(cat /mnt/tmp/install/data/variables/HEADERUUID) 
CODENAME=$(cat /mnt/tmp/install/data/variables/CODENAME)

mkdir -pv /mnt/tmp/install/data/dynamic/
cp -vf /etc/apt/sources.list /etc/apt/sources.list.d/debian-${CODENAME}.list

cat > /etc/apt/sources.list << HEREDOC
deb [signed-by=/etc/apt/keyrings/kali.gpg] http://http.kali.org/kali kali-rolling main contrib non-free non-free-firmware
deb-src [signed-by=/etc/apt/keyrings/kali.gpg] http://http.kali.org/kali kali-rolling main contrib non-free non-free-firmware
deb [signed-by=/etc/apt/keyrings/kali.gpg] http://http.kali.org/kali kali-last-snapshot main non-free contrib non-free-firmware
HEREDOC

while true;do
wget -qO- --directory-prefix=/usr/share/keyrings/ https://archive.kali.org/archive-key.asc | gpg --dearmor > /etc/apt/keyrings/kali.gpg && break
done
gpg --import /etc/apt/keyrings/kali.gpg
gpg --keyserver hkp://keys.gnupg.net --recv-key 44C6513A8E4FB3D30875F758ED444FF07D8D0BF6

apt update
apt dist-upgrade -y
apt full-upgrade -y
apt -y autoremove --purge
apt install -y kali-linux-everything  kali-desktop-xfce

cat > /etc/apt/sources.list.d/debian-${CODENAME}.list << HEREDOC
# deb https://deb.debian.org/debian/ ${CODENAME} main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian/ ${CODENAME} main contrib non-free non-free-firmware

# deb https://security.debian.org/debian-security ${CODENAME}-security main contrib non-free non-free-firmware
# deb-src https://security.debian.org/debian-security ${CODENAME}-security main contrib non-free non-free-firmware

# deb https://deb.debian.org/debian/ ${CODENAME}-updates main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian/ ${CODENAME}-updates main contrib non-free non-free-firmware
HEREDOC
