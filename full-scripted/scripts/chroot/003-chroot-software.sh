#!/bin/bash

# Exit on error
# set -e 

### Recuperation des variables
MACHINE=$(cat /mnt/tmp/install/data/variables/MACHINE)
USERNAME=$(cat /mnt/tmp/install/data/variables/USERNAME)
DEVBOOT=$(cat  /mnt/tmp/install/data/variables/DEVBOOT)
DEVSYST=$(cat /mnt/tmp/install/data/variables/DEVSYST) 
ROOTUUID=$(cat /mnt/tmp/install/data/variables/ROOTUUID) 
SWAPUUID=$(cat /mnt/tmp/install/data/variables/SWAPUUID) 
EFIUUID=$(cat /mnt/tmp/install/data/variables/EFIUUID) 
BOOTUUID=$(cat /mnt/tmp/install/data/variables/BOOTUUID) 
HEADERUUID=$(cat /mnt/tmp/install/data/variables/HEADERUUID) 
CODENAME=$(cat /mnt/tmp/install/data/variables/CODENAME)

# Element repository	
while true;do
	wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg && break
done	
gpg --import /usr/share/keyrings/element-io-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list

# Signal-desktop repository
while true;do
	wget -O- --directory-prefix=/usr/share/keyrings/ https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > /usr/share/keyrings/signal-desktop-keyring.gpg && break
done
gpg --import /usr/share/keyrings/signal-desktop-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | tee -a /etc/apt/sources.list.d/signal-xenial.list

# Mullvad
mkdir -pv /mnt/tmp/install/data/dynamic/vpn
while true;do
	wget -T 15 -c --directory-prefix=/mnt/tmp/install/data/dynamic/vpn/ --content-disposition https://mullvad.net/download/app/deb/latest && break
done
while true;do
	wget -T 15 -c --directory-prefix=/mnt/tmp/install/data/dynamic/vpn/ --content-disposition https://mullvad.net/download/app/deb/latest/signature && break
done
while true;do
wget -T 15 -c --directory-prefix=/mnt/tmp/install/data/dynamic/vpn/ https://mullvad.net/media/mullvad-code-signing.asc && break
done
gpg --import /mnt/tmp/install/data/dynamic/vpn/*.asc  
gpg --verify ./mnt/tmp/install/data/dynamic/vpn/*.deb.asc

echo -e "${BOLD}Le paquet d'installation de Mullvad semble authentique !${STD}"
echo -e "${BOLD}Installation du VPN${STD}"

# Visual studio code
while true;do
wget -qO- --directory-prefix=/usr/share/keyrings/ https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /etc/apt/keyrings/packages.microsoft.gpg && break
done
gpg --import /etc/apt/keyrings/packages.microsoft.gpg
echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list

# Install packages
apt update
apt install -y apt-transport-https
apt install -y ./mnt/tmp/install/data/dynamic/vpn/*.deb

apt install -y vlc libreoffice code keepassx audacity torbrowser-launcher thunderbird element-desktop signal-desktop tor firefox-esr gimp mat2 macchanger clementine mat2 vlc keepassx

