#!/bin/bash

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

### Recuperation des variables
MACHINE=$(cat /mnt/tmp/install/data/variables/MACHINE)
USERNAME=$(cat /mnt/tmp/install/data/variables/USERNAME)
DEVBOOT=$(cat  /mnt/tmp/install/data/variables/DEVBOOT)
DEVSYST=$(cat /mnt/tmp/install/data/variables/DEVSYST) 
ROOTUUID=$(cat /mnt/tmp/install/data/variables/ROOTUUID) 
SWAPUUID=$(cat /mnt/tmp/install/data/variables/SWAPUUID) 
EFIUUID=$(cat /mnt/tmp/install/data/variables/EFIUUID) 
BOOTUUID=$(cat /mnt/tmp/install/data/variables/BOOTUUID) 
HEADERUUID=$(cat /mnt/tmp/install/data/variables/HEADERUUID) 

### Chroot into newly installed system.

echo -e "${RED}Chroot new system.${STD}" 

./mnt/tmp/install/scripts/001-chroot-system.sh
#./mnt/tmp/install/scripts/002-chroot-distrib.sh
#./mnt/tmp/install/scripts/003-chroot-software.sh
#./mnt/tmp/install/scripts/004-chroot-config.sh

apt --fix-broken install -y
apt autoremove

update-initramfs -c -k all

sync

