#!/bin/bash

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

### Recuperation des variables
MACHINE=$(cat /mnt/tmp/install/data/variables/MACHINE)
USERNAME=$(cat /mnt/tmp/install/data/variables/USERNAME)
DEVBOOT=$(cat  /mnt/tmp/install/data/variables/DEVBOOT)
DEVSYST=$(cat /mnt/tmp/install/data/variables/DEVSYST) 
ROOTUUID=$(cat /mnt/tmp/install/data/variables/ROOTUUID) 
SWAPUUID=$(cat /mnt/tmp/install/data/variables/SWAPUUID) 
EFIUUID=$(cat /mnt/tmp/install/data/variables/EFIUUID) 
BOOTUUID=$(cat /mnt/tmp/install/data/variables/BOOTUUID) 
HEADERUUID=$(cat /mnt/tmp/install/data/variables/HEADERUUID) 

### Set hostname :
# echo ${MACHINE} > /etc/hostname

### Set locales :
#cat > /etc/default/locale << HEREDOC
#LANG="fr_FR.UTF-8"
#LANGUAGE=""
#LC_MESSAGES="fr_FR.UTF-8"
#COUNTRY="FR"
#HEREDOC
cat /mnt/tmp/install/config/locale > /etc/default/locale

### Update /etc/apt/sources.list from data/static/config/sources.list :
apt install -y lsb-release locales-all
CODENAME=$(lsb_release --codename --short)
echo ${CODENAME} > /mnt/tmp/install/data/variables/CODENAME
cat > /etc/apt/sources.list << HEREDOC
deb https://deb.debian.org/debian/ ${CODENAME} main contrib non-free non-free-firmware
deb-src https://deb.debian.org/debian/ ${CODENAME} main contrib non-free non-free-firmware

deb https://security.debian.org/debian-security ${CODENAME}-security main contrib non-free non-free-firmware
deb-src https://security.debian.org/debian-security ${CODENAME}-security main contrib non-free non-free-firmware

deb https://deb.debian.org/debian/ ${CODENAME}-updates main contrib non-free non-free-firmware
deb-src https://deb.debian.org/debian/ ${CODENAME}-updates main contrib non-free non-free-firmware
HEREDOC

### Copy content of data/static/config/sources.list.d/ from /etc/apt/sources.list.d/ :
cp -rf /mnt/tmp/install/config/sources.list.d/* /etc/apt/sources.list.d/
chown -R root:root /etc/apt/sources.list.d/*
chmod -r 644 /etc/apt/sources.list.d/*

### Update
apt update

### Minimun install
apt install -y linux-image-amd64 linux-headers-amd64 lvm2 cryptsetup cryptsetup-initramfs grub2-common grub-efi firmware-linux firmware-linux-nonfree curl wget gnupg firmware-realtek firmware-iwlwifi locales-all sudo 

### Create root user 
echo -e "${BOLD}Création du compte d'administration.${STD}"
echo
echo -e "${RED}Choisissez le mot de passe root :${STD}"
echo
passwd root

### Create an unprivileged user with sudo access :
echo -e "${BOLD}Création du compte ${USERNAME}.${STD}"
echo
useradd  -D -m -G sudo ${USERNAME}
echo -e "${RED}Choisissez le mot de passe de ${USERNAME} :${STD}"
echo
passwd ${USERNAME}

mkdir -pv /home/${USERNAME}/setup/
cp -vrf /mnt/tmp/documents/* /home/${USERNAME}/setup/

# Enable initramfs modules (not sure if necessary)
# echo "aes-i586" >> /etc/initramfs-tools/modules
# echo "dm-crypt" >> /etc/initramfs-tools/modules
# echo "dm-mod" >> /etc/initramfs-tools/modules
# echo "sha256" >> /etc/initramfs-tools/modules

# https://www.debuntu.org/how-to-encrypted-partitions-over-lvm-with-luks-page-3-install-and-config/
# Take reference from data/static/ressources/native-schema

# /etc/modules-load.d/modules.conf
# cat > /etc/modules-load.d/modules.conf<< HEREDOC
# vfat
# nls_cp437
# nls_ascii
# HEREDOC

# /etc/fstab
cat > /etc/fstab << HEREDOC
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/dev/mapper/${MACHINE}----vg-root /               ext4    errors=remount-ro 0       1
# UUID=${ROOTUUID} /               ext4    errors=remount-ro 0       1

# /boot was on /dev/${BOOTUUID} during installation
UUID=${BOOTUUID} /boot           ext4    defaults        0       2

# /boot/efi was on /dev/${EFIUUID} during installation
UUID=${EFIUUID}  /boot/efi       vfat    umask=0077      0       1

/dev/mapper/${MACHINE}----vg-swap none            swap    sw              0       0
# UUID=${SWAPUUID} none            swap    sw              0       0
HEREDOC

# /etc/crypttab
cat > /etc/crypttab << HEREDOC
${DEVSYST}_decrypt /dev/${DEVSYST} none luks,discard,header=/dev/sda3
# Header in file version :
# ${DEVSYST}_decrypt /dev/${DEVSYST} none luks,discard,header=/luks/header.luks:UUID=${HEADERUUID}
HEREDOC

### Install boot loader to boot disk :
grub-install /dev/${DEVBOOT} --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=nohead --recheck --no-nvram --removable
update-grub 

### Regenerate initramfs
update-initramfs -c -k all
