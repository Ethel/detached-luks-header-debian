#!/bin/bash 

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

## Install Debian
# conditionner a la reussite sinon relancer
debootstrap --arch=amd64 stable target/ https://deb.debian.org/debian

## Configure for chroot

### Bind mount a few directories and chroot into the new system to do some setup

mkdir -pv target/{dev,sys,proc,mnt,bin,lib,lib64,lib32}
mkdir -pv target/dev/pts
mkdir -pv target/mnt/tmp

cp -alf /bin target/bin
cp -alf /lib target/lib
cp -alf /lib target/lib32
cp -alf /lib target/lib64
mount -o bind /dev target/dev
mount -o bind /dev/pts target/dev/pts
mount -o bind /sys target/sys
mount -o bind /proc target/proc
mount -t tmpfs tmpfs target/mnt/tmp

### Copy config file to tmp
mkdir -pv target/mnt/tmp/install/{scripts,data}
mkdir -pv target/mnt/tmp/install/data/packages
mkdir -pv target/mnt/tmp/documents
cp -vrf data/static/config target/mnt/tmp/install/
cp -vrf data/dynamic/variables target/mnt/tmp/install/data/
cp -vrf scripts/chroot/* target/mnt/tmp/install/scripts/
cp -vrf documents/* target/mnt/tmp/documents/
