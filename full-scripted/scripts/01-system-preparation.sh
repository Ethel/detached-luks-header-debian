#!/bin/bash	

## Ressources :
### https://gist.github.com/Lakshmipathi/81fe57c9507304fa5adbaa4fb2feaed7#bios-systems
### https://maple52046.github.io/posts/install-ubuntu-in-uefi-mode-with-debootstrap/

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

### Remove traces of old or broken install
sudo umount -Rv target
rm -rvf target/
rm -rvf data/dynamic/packages/*
rm -rvf data/dynamic/variables/*

### Install haveged to make more entropy available, debootstrap to install debian :
apt install -y haveged debootstrap gddrescue

CODENAME=$(lsb_release --codename --short)
if [[ $CODENAME = "kali-rolling" ]]
	then
		while true;do
			wget -T 15 -c --directory-prefix=data/dynamic/packages/ http://ftp.debian.org/debian/pool/main/g/gddrescue/gddrescue_1.27-1_amd64.deb && break
		done

		while true;do
			wget -T 15 -c --directory-prefix=data/dynamic/packages/ https://deb.debian.org/debian/pool/main/d/debootstrap/debootstrap_1.0.128+nmu2~bpo11+1_all.deb && break
		done

		sudo dpkg -i data/dynamic/packages/gddrescue_1.27-1_amd64.deb 
		sudo dpkg -i data/dynamic/packages/debootstrap_1.0.128+nmu2\~bpo11+1_all.deb     

fi


### Create target directory
mkdir -pv target/
sudo chown -R 1000:1000 target/
	
### One hard drive to receive the system. The letter identifying the drive will be stored in the variable DEVSYST.

lsblk

while :
do
	if [[ $DEVSYSTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est la lettre du device du futur disque système ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVSYST
touch data/dynamic/variables/DEVSYST
echo ${DEVSYST} >> data/dynamic/variables/DEVSYST
	echo
	echo -e "${BOLD}Vous avez choisi d'installer votre système sur /dev/$DEVSYST, êtes vous sûr.e de votre choix ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVSYSTCHOICE
done

### One usb device to receive the bootloader and the LUKS header. The letter identifying the drive will be stored in the variable DEVBOOT.
while :
do
	if [[ $DEVBOOTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est la lettre de la future clé de boot ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVBOOT
touch data/dynamic/variables/DEVBOOT
echo ${DEVBOOT} >> data/dynamic/variables/DEVBOOT
	echo
	echo -e "${BOLD}Vous avez choisi de creer la clé de boot sur /dev/$DEVBOOT, êtes vous sûr.e de votre choix ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVBOOTCHOICE
done


### Choose a name for your computer (it will be used to name the LVM volumes too)
echo -e "${BOLD}Saisissez le nom que vous voulez donner à votre future machine.${STD}"
read MACHINE
touch data/dynamic/variables/MACHINE
echo ${MACHINE} >> data/dynamic/variables/MACHINE

### Choose a name for your user 
echo -e "${BOLD}Saisissez le nom que vous voulez donner à votre utilisateur.${STD}"
read USERNAME
touch data/dynamic/variables/USERNAME
echo ${USERNAME} >> data/dynamic/variables/USERNAME

### Flush cache :
sync 

### Clean up of the disks :
echo -e "${BOLD}Do you need to shred the future ${RED}boot${STD} drive ? It may take some time (Y/n)${STD}"
echo
read -p "(Default Y)" -n 1 -r SHREDBOOTYN

echo -e "${BOLD}Do you need to shred the future ${RED}system${STD} drive ? It may take some time (Y/n)${STD}"
echo
read -p "(Default Y)" -n 1 -r SHREDSYSTYN

if [[ ! ${SHREDBOOTYN} =~ ^[Nn]$ ]]
then
	shred -f -n3 -v /dev/${DEVBOOT}
	sync
	echo
	echo -e "${BOLD}/dev/${DEVBOOT} has been shredded.${STD}"
fi

if [[ ! ${SHREDSYSTYN} =~ ^[Nn]$ ]]
then
	shred -f -n3 -v /dev/${DEVSYST}
	sync
	echo
	echo -e "${BOLD}/dev/${DEVSYST} has been shredded.${STD}"
fi


## Creation of the partitions.
### Empty the disks :
sudo sfdisk --delete /dev/${DEVBOOT}
sudo sfdisk --delete /dev/${DEVSYST}
sync


### Create the partitions on the boot disk :
#### (Non fonctionnel) Make a partition ${DEVBOOT}1 for /efi  and ${DEVBOOT}2 for /boot from sfdisk script file : sudo sfdisk /dev/${DEVBOOT} < data/static/install/bootdisk.sfdisk 
# Header in file version :
# sudo fdisk /dev/${DEVBOOT} < data/static/install/fdisk-commands
# Header in partion version :
sudo fdisk /dev/${DEVBOOT} < data/static/install/fdisk-commands-header-partition

### Format the /dev/${DEVBOOT}1 efi partition :
sudo mkfs.vfat -F 32 /dev/${DEVBOOT}1

### Format the /dev/${DEVBOOT}2 boot partition with ext2 :
sudo mkfs.ext2 -c -L boot -m 0 /dev/${DEVBOOT}2

### Create temporary folders to mount filesystems for the installation
mkdir -pv target/boot/
sudo chown -R 1000:1000 target/boot/

### Mount /dev/${DEVBOOT}2 to receive the LUKS header :
mount /dev/${DEVBOOT}2 target/boot/

mkdir -pv target/boot/{efi,luks}
sudo chown -R 1000:1000 target/boot/

### Mount /dev/${DEVBOOT}1 to receive the EFI :
mount /dev/${DEVBOOT}1 target/boot/efi/

### Encrypt the system drive. Many ciphers are available, (check for them via /proc/crypto). Not working since it needs initramfs to be able to mount filesystem. 
# Header in file version :
# sudo cryptsetup -v --use-random --verify-passphrase --cipher aes-xts-benbi --key-size=512 --hash=sha512 --iter-time=10000 --header target/boot/luks/header.luks luksFormat /dev/${DEVSYST}
# Header in partition version :
mkdir -pv data/dynamic/luks/luks-header
mount -t tmpfs -o size=512m tmpfs data/dynamic/luks/luks-header
sudo cryptsetup -v --use-random --verify-passphrase --cipher aes-xts-benbi --key-size=512 --hash=sha512 --iter-time=10000 --header data/dynamic/luks/luks-header/header.luks luksFormat /dev/${DEVSYST}
dd if=data/dynamic/luks/luks-header/header.luks of=/dev/${DEVBOOT}3 status=progress && sync
# Clean up of tmpfs is in clean-up script

### Make available the decrypted drive.
# Header in file version :
# sudo cryptsetup luksOpen --header target/boot/luks/header.luks /dev/${DEVSYST} ${DEVSYST}_decrypt
# Header in partition version :
sudo cryptsetup luksOpen --header data/dynamic/luks/luks-header/header.luks /dev/${DEVSYST} ${DEVSYST}_decrypt

### Prepare disk or partition for LVM use.
sudo pvcreate /dev/mapper/${DEVSYST}_decrypt

### Create LVM volume group.
sudo vgcreate ${MACHINE}--vg /dev/mapper/${DEVSYST}_decrypt

### Create LVM logical volume groups.
sudo lvcreate -L 2G -n swap ${MACHINE}--vg
sudo lvcreate -l 100%FREE -n root ${MACHINE}--vg

### Format logical volume partitions.
sudo mkfs.ext4 -L root /dev/mapper/${MACHINE}----vg-root
sudo mkswap -L swap /dev/mapper/${MACHINE}----vg-swap

### Umount and remove folders to make room for mounting /dev/${DEVSYST}
sync
umount target/boot/efi
umount target/boot
rm -rvf target/*

### Mount filesystems to prepare for installation.
sudo mount /dev/mapper/${MACHINE}----vg-root target/ 
sudo swapon /dev/mapper/${MACHINE}----vg-swap

### Mount /dev/${DEVBOOT}2 to /boot/ :
mkdir -pv target/boot
mount /dev/${DEVBOOT}2 target/boot/

mkdir -pv target/boot/{efi,luks}
sudo chown -R 1000:1000 target/boot/

### Mount /dev/${DEVBOOT}1 to /boot/efi/ :
mount /dev/${DEVBOOT}1 target/boot/efi/

touch data/dynamic/variables/USERNAME
echo ${USERNAME} >> data/dynamic/variables/USERNAME

### Stock UUIDS for future fstab an crypttab config
# vg----root
lsblk -nr -o UUID,NAME | grep "${MACHINE}----vg-root" | grep -Po '.*' > data/dynamic/variables/TMPUUID
cat data/dynamic/variables/TMPUUID | grep -G -o '\S*' | head -n1 > data/dynamic/variables/ROOTUUID
ROOTUUID=$(cat data/dynamic/variables/ROOTUUID)

# vg----swap
lsblk -nr -o UUID,NAME | grep "${MACHINE}----vg-root" | grep -Po '.*' > data/dynamic/variables/TMPUUID
cat data/dynamic/variables/TMPUUID | grep -G -o '\S*' | head -n1 > data/dynamic/variables/SWAPUUID
SWAPUUID=$(cat data/dynamic/variables/SWAPUUID)

# /boot/efi
lsblk -nr -o UUID,NAME | grep "${DEVBOOT}1" | grep -Po '.*' > data/dynamic/variables/TMPUUID
cat data/dynamic/variables/TMPUUID | grep -G -o '\S*' | head -n1 > data/dynamic/variables/EFIUUID
EFIUUID=$(cat data/dynamic/variables/EFIUUID)

# /boot
lsblk -nr -o UUID,NAME | grep "${DEVBOOT}2" | grep -Po '.*' > data/dynamic/variables/TMPUUID
cat data/dynamic/variables/TMPUUID | grep -G -o '\S*' | head -n1 > data/dynamic/variables/BOOTUUID
BOOTUUID=$(cat data/dynamic/variables/BOOTUUID)

# header
lsblk -nr -o UUID,NAME | grep "${DEVBOOT}3" | grep -Po '.*' > data/dynamic/variables/TMPUUID
cat data/dynamic/variables/TMPUUID | grep -G -o '\S*' | head -n1 > data/dynamic/variables/HEADERUUID
HEADERUUID=$(cat data/dynamic/variables/TMPUUID)

rm -vf data/dynamic/variables/TMPUUID

# https://wiki.archlinux.org/title/Dm-crypt/Specialties#Encrypted_system_using_a_detached_LUKS_header
