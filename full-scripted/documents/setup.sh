#!/bin/bash	

# Variables de mise en page :
RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

## Prerequisites :
# Check sudo or exit 

if (( $EUID != 0 )); then
    echo -e "${RED}Le script semble ne pas avoir été lancé avec sudo. # Echec.${STD}" && exit 1
fi

# Enable macchanger
ifconfig wlan0 down
ifconfig eth0 down
macchanger -r wlan0
macchanger -r eth0
ifconfig wlan0 up
ifconfig eth0 up

systemctl enable changemac@wlan0.service
systemctl enable changemac@eth0.service

macchanger -s wlan0
macchanger -s eth0


# Set up Mullvad VPN
echo -e "${BOLD}Saisissez votre login Mullvad (16 chiffres sans espace)${STD}"

MULLVADACCOUNT=$(cat ~/svg/MULLVADACCOUNT)

mullvad account login "$MULLVADACCOUNT"
mullvad auto-connect set on
mullvad lockdown-mode set on
mullvad auto-connect get
mullvad lockdown-mode get
mullvad relay set location ch

echo -e "${BOLD}Connection au VPN${STD}"
mullvad connect
mullvad status -v

# Google Earth
while true;do
	wget -T 15 -c --directory-prefix=/mnt/tmp/install/data/dynamic/ https://dl.google.com/dl/earth/client/current/google-earth-pro-stable_current_amd64.deb && break
done
dpkg -i /mnt/tmp/install/data/dynamic/google-earth-pro-stable_current_amd64.deb
