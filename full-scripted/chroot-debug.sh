#!/bin/bash	

# Variables de mise en page :
RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

## Prerequisites :
# Check sudo or exit 

if (( $EUID != 0 )); then
    echo -e "${RED}Le script semble ne pas avoir été lancé avec sudo. # Echec.${STD}" && exit 1
fi


### Remove traces of old or broken install
sudo umount -Rv target
rm -rvf target/
rm -rvf data/dynamic/packages/*
rm -rvf data/dynamic/variables/*

### Create target directory
mkdir -pv target/
sudo chown -R 1000:1000 target/

lsblk

while :
do
	if [[ $DEVSYSTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est la lettre du device du disque système ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVSYST
touch data/dynamic/variables/DEVSYST
echo ${DEVSYST} >> data/dynamic/variables/DEVSYST
	echo
	echo -e "${BOLD}Le système est-il bien sur /dev/$DEVSYST ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVSYSTCHOICE
done

### One usb device to receive the bootloader and the LUKS header. The letter identifying the drive will be stored in the variable DEVBOOT.
while :
do
	if [[ $DEVBOOTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est la lettre de la clé de boot ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVBOOT
touch data/dynamic/variables/DEVBOOT
echo ${DEVBOOT} >> data/dynamic/variables/DEVBOOT
	echo
	echo -e "${BOLD}La clé de boot est-elle bien sur /dev/$DEVBOOT ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVBOOTCHOICE
done

### Create temporary drive to access luks header
mkdir -pv tmp/boot/
sudo chown -R 1000:1000 tmp/
### Mount /dev/${DEVBOOT}2 to tmpboot :
mount /dev/${DEVBOOT}2 tmp/boot/

### Make available the decrypted drive.
sudo cryptsetup luksOpen --header tmp/boot/luks/header.luks /dev/${DEVSYST} ${DEVSYST}_decrypt

while :
do
	if [[ $MACHINECHOICE = "YES" ]]; then
    		break
	fi
echo -e "${BOLD}Quel est le nom des volumes LVM (le 'xxx' de 'xxx----vg-root' et 'xxx----vg-swap')${STD}"
read -p : -r MACHINE
touch data/dynamic/variables/MACHINE
echo ${MACHINE} >> data/dynamic/variables/MACHINE
	echo
	echo -e "${BOLD}Le nom de volume est bien ${MACHINE} ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r MACHINECHOICE
done

### Mount filesystems to prepare for installation.
sudo mount /dev/mapper/${MACHINE}----vg-root target/ 
sudo swapon /dev/mapper/${MACHINE}----vg-swap

### Create temporary folders to mount filesystems for the installation
mkdir -pv target/boot/
sudo chown -R 1000:1000 target/boot/

### Mount /dev/${DEVBOOT}2 to receive the LUKS header :
### Remove tmpboot
umount tmp/boot
rm -rf tmp/boot/
mount /dev/${DEVBOOT}2 target/boot/

mkdir -pv target/boot/{efi,luks}
sudo chown -R 1000:1000 target/boot/

### Mount /dev/${DEVBOOT}1 to receive the EFI :
mount /dev/${DEVBOOT}1 target/boot/efi/

## Configure for chroot
### Bind mount a few directories and chroot into the new system to do some setup
mkdir -pv target/{dev,sys,proc,mnt}
mkdir -pv target/dev/pts
mkdir -pv target/mnt/tmp

mount -o bind /dev target/dev
mount -o bind /dev/pts target/dev/pts
mount -o bind /sys target/sys
mount -o bind /proc target/proc
mount -t tmpfs tmpfs target/mnt/tmp
mkdir -pv target/mnt/tmp/install/{scripts,data}
mkdir -pv target/mnt/tmp/install/data/packages
### Copy config file to tmp
cp -vrf data/static/config target/mnt/tmp/install/
cp -vrf data/dynamic/variables target/mnt/tmp/install/data/
cp -vrf scripts/chroot/* target/mnt/tmp/install/scripts/

# Chroot into target
sudo chroot target/ /bin/bash

### Recuperation des variables
MACHINE=$(cat /mnt/tmp/install/data/variables/MACHINE)
USERNAME=$(cat /mnt/tmp/install/data/variables/USERNAME)
DEVBOOT=$(cat  /mnt/tmp/install/data/variables/DEVBOOT)
DEVSYST=$(cat /mnt/tmp/install/data/variables/DEVSYST) 
ROOTUUID=$(cat /mnt/tmp/install/data/variables/ROOTUUID) 
SWAPUUID=$(cat /mnt/tmp/install/data/variables/SWAPUUID) 
EFIUUID=$(cat /mnt/tmp/install/data/variables/EFIUUID) 
BOOTUUID=$(cat /mnt/tmp/install/data/variables/BOOTUUID) 
HEADERUUID=$(cat /mnt/tmp/install/data/variables/HEADERUUID) 


# sudo umount target/dev/pts
# sudo umount target/sys
# sudo umount target/proc
# sudo umount target/mnt/tmp
# sudo umount target/boot/efi
# sudo umount target/boot
# sudo umount target/dev

# sudo lvchange -an /dev/${MACHINE}--vg/swap
# sudo lvchange -an /dev/${MACHINE}--vg/root
# sudo crypsetup luksClose ${DEVSYST}_decrypt
# sudo rm -rf target
