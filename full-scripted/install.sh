#!/bin/bash	

# Variables de mise en page :
RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

## Prerequisites :
# Check sudo or exit 

if (( $EUID != 0 )); then
    echo -e "${RED}Le script semble ne pas avoir été lancé avec sudo. # Echec.${STD}" && exit 1
fi

sudo ./scripts/01-system-preparation.sh

### Recuperation des variables
MACHINE=$(cat data/dynamic/variables/MACHINE)
USERNAME=$(cat data/dynamic/variables/USERNAME)
DEVBOOT=$(cat  data/dynamic/variables/DEVBOOT)
DEVSYST=$(cat data/dynamic/variables/DEVSYST) 
ROOTUUID=$(cat data/dynamic/variables/ROOTUUID) 
SWAPUUID=$(cat data/dynamic/variables/SWAPUUID) 
EFIUUID=$(cat data/dynamic/variables/EFIUUID) 
BOOTUUID=$(cat data/dynamic/variables/BOOTUUID) 
HEADERUUID=$(cat data/dynamic/variables/HEADERUUID) 

sudo ./scripts/02-system-installation.sh $MACHINE $USERNAME $DEVBOOT $DEVSYST $ROOTUUID $SWAPUUID $EFIUUID $BOOTUUID $HEADERUUID

sudo chroot target/ /bin/bash mnt/tmp/install/scripts/install-chroot.sh
sync 
# sudo ./scripts/03-clean-up.sh

echo -e "${RED}L'installation est finie, vous pouvez redémarrer sur votre nouveau système.${STD}"
