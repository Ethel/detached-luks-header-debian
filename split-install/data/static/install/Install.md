Boot up the Debian installer, select Advanced options... and then Expert install. 

Continue step by steo through until you get to Load installer components from CD. From here, you want to select crypto-dm-modules and rescue-mode.

Continue working through the installer, but stop before clicking on Detect disks. Instead, hit `ctrl+alt+f2` to jump into a shell.

We can load the necessary modules from here.

```console
depmod -a
```

Next, use cryptsetup to unlock your lvm on luks device. It takes the device and a friendly name. In my example, these are /dev/sda3 and volume-decrypt.

```console
cryptsetup luksOpen /dev/sda3 volume-crypt
```
and `Enter passphrase for /dev/sda3:`

If this goes well, `/dev/mapper/volume-decrypt` should now be available. Let's open the volume group.

```console
vgchange -ay
```

We should see our logical volumes and our crypt device. In my case:

```console
ls /dev/mapper
```
shows `control debian-decrypt debian-home debian-root debian-swap`

Hit `ctrl+alt+f1` or `ctrl+alt+f1` to return to the Debian installer and continue to Partition disks. Select Manual. We should now see all of our volumes created before.

Enter the `swap` partition and configure it to be used as swap (it should be already the case).

Enter the `ext4` partition and configure it to be used as `ext4`, tell the installer to remove data and to use it as `/`.

Enter the `ext2` partition and configure it to be used as ext2, tell the installer to remove data and to use it as `/boot`

Enter the ESP partition and configure it to be used as EFI, (it should be already the case).

Then finish partitionning and write changes to disk.

If all looks good, select Yes and continue through the base installer, stopping before Install the GRUB bootloader to a hard disk. Again, hit `ctrl+alt+f2` to jump into a terminal.

```console
nano /target/etc/crypttab
```
and modify the document like this : 

```conf
# <target name> <source device>		<key file>	<options>
${MACHINE}-decrypt /dev/${DEVSYST} none luks,header=/dev/${DEVBOOT}3
