#!/bin/bash	

# Variables de mise en page :
RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

## Prerequisites :
# Check sudo or exit 
if (( $EUID != 0 )); then
    echo -e "${RED}Le script semble ne pas avoir été lancé avec sudo. # Echec.${STD}" && exit 1
fi

tar --exclude="luks-nohead.tar.gz" -czvf luks-nohead.tar.gz .

sudo ./scripts/01-01-system-preparation.sh
DEVINSTYN=$(cat data/dynamic/variables/DEVINSTYN)
if [[ $DEVINSTYN = "YES" ]]; then
	sudo ./scripts/01-02-01-create-kali-live.sh
fi 
sudo ./scripts/01-03-config-preparation.sh

sync 
echo -e "${BOLD}Notez les deux lignes suivantes elles vous serviront pour la suite.${STD}"
echo -e "(Elles sont stockées dans le repertoire documents/ de votre clé de boot)"
echo
cat cryptsetup.command
cat crypttab.line
echo
echo -e "${BOLD}Le système de fichier est prêt, rebootez sur la clé d'installation et choisissez ${RED}installer en mode expert${STD}. Ouvrez le document documents/Install.md présent sur la clé de boot sur un autre appareil pour suivre les etapes.${STD}"



