#!/bin/bash	

# Variables de mise en page :
RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

## Prerequisites :
# Check sudo or exit 

if (( $EUID != 0 )); then
    echo -e "${RED}Le script semble ne pas avoir été lancé avec sudo. # Echec.${STD}" && exit 1
fi

sudo ./scripts/02-01-config-before-internet.sh

# Configuration 
echo
echo -e "${BOLD}Une fois connecté, souhaitez-vous continuer la configuration de kali ?(Y/n)${STD}"
read -p "(Par défaut Y)" -n 1 -r CONTINUEYN

if [[ ! $CONTINUEYN =~ ^[Nn]$ ]]
then
	echo -e "${BOLD}C'est parti.${STD}"
else
	exit 0
fi

# Test connectivity and continue if connected
while :
do
	ping -c 1 -W 5 mullvad.net 1>/dev/null 2>&1 
	if [ $? -eq 0 ]
	then 
		echo "${BOLD}Connection établie !${STD}"
		break
	else 
		echo "${RED}Pour continuer la configuration de kali-live, vous devez vous connecter à internet. ${BOLD}Vous pouvez le faire en gardant cette fenêtre ouverte.${STD}"
		sleep 2
fi
done

# Installation de Mullvad-VPN
echo
echo -e "${BOLD}Souhaitez-vous installer mullvad-vpn ?(Y/n)${STD}"
read -p "(Par défaut Y)" -n 1 -r MULLVADYN

# Installation de Riseup-VPN
echo
echo -e "${BOLD}Souhaitez-vous installer riseup-vpn ?(Y/n)${STD}"
read -p "(Par défaut Y)" -n 1 -r RISEUPYN

# Installation des autres paquets
echo
echo -e "${BOLD}Souhaitez-vous installer d'autres logiciels ?(Y/n)${STD}"
read -p "(Par défaut Y)" -n 1 -r OTHERYN

if [[ ! $MULLVADYN =~ ^[Nn]$ ]]
then
	./scripts/02-02-01-install-mullvad-vpn.sh
fi	

if [[ ! $RISEUPYN =~ ^[Nn]$ ]]
then
	./scripts/02-02-02-install-riseup-vpn.sh
fi	

if [[ ! $OTHERYN =~ ^[Nn]$ ]]
then
sudo ./scripts/02-03-install-more-things.sh
fi

sudo ./scripts/02-04-clean-up.sh
sync 
echo -e "${RED}L'installation est finie. Vous pouvez fermer ce terminal.${STD}"
