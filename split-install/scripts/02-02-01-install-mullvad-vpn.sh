#!/bin/bash

# Variables de mise en page

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Get Mullvad .deb name
mkdir -pv data/dynamic/vpn
wget --spider  https://mullvad.net/download/app/deb/latest/ 2> data/dynamic/vpn/mullvad-version
MULLVAD=$(cat data/dynamic/vpn/mullvad-version | grep -G -o MullvadVPN-202[0-9].[0-9]_amd64.deb | head -n1)
echo $MULLVAD


# Check if Mullvad is already there and download it if not

if [ -f "data/dynamic/vpn/$MULLVAD" ]; 
  	then
 	echo -e "${BOLD}Mullvad est déjà présent, pas besoin de le télécharger !${STD}"
	else
	echo -e "${BOLD}Téléchargement de Mullvad.${STD}"

	while true;do
		wget -T 15 -c --directory-prefix=data/dynamic/vpn --content-disposition https://mullvad.net/download/app/deb/latest && break
	done
	echo -e "${BOLD}Mullvad a été téléchargé avec succès !${STD}"
fi

echo -e "${BOLD}Vérification de l'authenticité de l'image de Mullvad.${STD}"

# Check GPG return code and exit if error

while true;do
	wget -T 15 -c --directory-prefix=data/dynamic/vpn --content-disposition https://mullvad.net/download/app/deb/latest/signature && break
done
while true;do
wget -T 15 -c --directory-prefix=data/dynamic/vpn https://mullvad.net/media/mullvad-code-signing.asc && break
done
gpg --import data/dynamic/vpn/*.asc  
gpg --verify ./data/dynamic/vpn/*.deb.asc
[[ $? -gt 0 ]] && echo -e "${RED}Verification echouée, relancez le script." && rm -vrf data/dynamic/vpn && exit 1

echo -e "${BOLD}Le paquet d'installation de Mullvad semble authentique !${STD}"
echo -e "${BOLD}Installation du VPN${STD}"

sudo apt install -y ./data/dynamic/vpn/$MULLVAD
[[ $? -gt 0 ]] && echo -e "${RED}Installation echouée, relancez le script." && exit 1

# Set up Mullvad VPN
echo -e "${BOLD}Saisissez votre login Mullvad (16 chiffres sans espace)${STD}"

read ACCOUNT

mullvad account login "$ACCOUNT"
mullvad auto-connect set on
mullvad lockdown-mode set on
mullvad auto-connect get
mullvad lockdown-mode get
mullvad relay set location ch

echo -e "${BOLD}Connection au VPN${STD}"

mullvad connect
mullvad status -v
