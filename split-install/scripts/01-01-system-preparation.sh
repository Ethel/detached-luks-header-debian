#!/bin/bash	

## Ressources :
### https://gist.github.com/Lakshmipathi/81fe57c9507304fa5adbaa4fb2feaed7#bios-systems


# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

### Remove traces of old or broken install
rm -rvf data/dynamic/packages/*
rm -rvf data/dynamic/variables/*

### Install haveged to make more entropy available
apt install -y haveged gddrescue

CODENAME=$(lsb_release --codename --short)
if [[ $CODENAME = "kali-rolling" ]]
	then
		while true;do
			wget -T 15 -c --directory-prefix=data/dynamic/packages/ http://ftp.debian.org/debian/pool/main/g/gddrescue/gddrescue_1.27-1_amd64.deb && break
		done
		sudo dpkg -i data/dynamic/packages/gddrescue_1.27-1_amd64.deb  
fi

### Create target directory
mkdir -pv target/
sudo chown -R 1000:1000 target/
	
### One hard drive to receive the system. The letter identifying the drive will be stored in the variable DEVSYST.

lsblk

while :
do
	if [[ $DEVSYSTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est le nom du device du futur disque système ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVSYST
touch data/dynamic/variables/DEVSYST
echo ${DEVSYST} >> data/dynamic/variables/DEVSYST
	echo
	echo -e "${BOLD}Vous avez choisi d'installer votre système sur /dev/$DEVSYST, êtes vous sûr.e de votre choix ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVSYSTCHOICE
done


### One usb device to receive the bootloader and the LUKS header. The letter identifying the drive will be stored in the variable DEVBOOT.
while :
do
	if [[ $DEVBOOTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est la lettre de la future clé de boot ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVBOOT
touch data/dynamic/variables/DEVBOOT
echo ${DEVBOOT} >> data/dynamic/variables/DEVBOOT
	echo
	echo -e "${BOLD}Vous avez choisi de creer la clé de boot sur /dev/$DEVBOOT, êtes vous sûr.e de votre choix ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVBOOTCHOICE
done

### One usb device to receive the live system to install DEVINST.
echo -e "${BOLD}Faut-il créer une clé live pour l'installation du système ? (Y/n)${STD}"
read -p "(Par défaut Y)" -n 1 -r DEVINSTYN


if [[ ! $DEVINSTYN =~ ^[Nn]$ ]]
then
while :
do
	if [[ $DEVINSTCHOICE = "YES" ]]; then
    		break
	fi

	echo -e "${BOLD}En se référant au tableau ci-dessus, quelle est le nom de la future clé d'installation ? (le 'xxx' de '/dev/xxx')${STD}"
read -p /dev/ -r DEVINST
touch data/dynamic/variables/DEVINST
echo ${DEVINST} >> data/dynamic/variables/DEVINST
touch data/dynamic/variables/DEVINSTYN
echo "YES" >> data/dynamic/variables/DEVINSTYN
	echo
	echo -e "${BOLD}Vous avez choisi de creer la clé live sur /dev/$DEVINST, êtes vous sûr.e de votre choix ? Si oui, écrivez YES en toutes lettres et appuyez sur entrée. Toute autre saisie vous proposera de nouveau de choisir le bon device.${STD}"
	read -r DEVINSTCHOICE
done
fi

### Choose a name for the LVM volumes
echo -e "${BOLD}Saisissez le nom que vous voulez donner à vos volumes LVM.${STD}"
read VOLUME
touch data/dynamic/variables/VOLUME
echo ${VOLUME} >> data/dynamic/variables/VOLUME

### Flush cache :
sync 

### Clean up of the disks :
echo -e "${BOLD}Do you need to shred the future ${RED}boot${STD} drive ? It may take some time (Y/n)${STD}"
echo
read -p "(Default Y)" -n 1 -r SHREDBOOTYN

echo -e "${BOLD}Do you need to shred the future ${RED}system${STD} drive ? It may take some time (Y/n)${STD}"
echo
read -p "(Default Y)" -n 1 -r SHREDSYSTYN

if [[ ! ${SHREDBOOTYN} =~ ^[Nn]$ ]]
then
	shred -f -n3 -v /dev/${DEVBOOT}
	sync
	echo
	echo -e "${BOLD}/dev/${DEVBOOT} has been shredded.${STD}"
fi

if [[ ! ${SHREDSYSTYN} =~ ^[Nn]$ ]]
then
	shred -f -n3 -v /dev/${DEVSYST}
	sync
	echo
	echo -e "${BOLD}/dev/${DEVSYST} has been shredded.${STD}"
fi


## Creation of the partitions.
### Empty the disks :
sudo sfdisk --delete /dev/${DEVBOOT}
sudo sfdisk --delete /dev/${DEVSYST}
sync


### Create the partitions on the boot disk :
#### (Non fonctionnel) Make a partition ${DEVBOOT}1 for /efi  and ${DEVBOOT}2 for /boot from sfdisk script file : sudo sfdisk /dev/${DEVBOOT} < data/static/install/bootdisk.sfdisk 
# Header in file version :
# sudo fdisk /dev/${DEVBOOT} < data/static/install/fdisk-commands
# Header in partion version :
sudo fdisk /dev/${DEVBOOT} < data/static/install/fdisk-commands-header-partition

### Format the /dev/${DEVBOOT}1 efi partition :
sudo mkfs.vfat -F 32 /dev/${DEVBOOT}1

### Format the /dev/${DEVBOOT}2 boot partition with ext2 :
sudo mkfs.ext2 -c -L boot -m 0 /dev/${DEVBOOT}2

### Format the /dev/${DEVBOOT}4 boot partition with ext4 :
sudo mkfs.ext4 -c -L documents -m 0 /dev/${DEVBOOT}4

### Mount /dev/${DEVBOOT}4 to receive data/dynamic/documents :
mount /dev/${DEVBOOT}4 target/ 
mkdir -pv target/documents/luks-header/

### Encrypt the system drive. Many ciphers are available, (check for them via /proc/crypto). Not working since it needs initramfs to be able to mount filesystem. 
# Header in file version :
# sudo cryptsetup -v --use-random --verify-passphrase --cipher aes-xts-benbi --key-size=512 --hash=sha512 --iter-time=10000 --header target/boot/luks/header.luks luksFormat /dev/${DEVSYST}
# Header in partition version :
sudo cryptsetup -v --use-random --verify-passphrase --cipher aes-xts-benbi --key-size=512 --hash=sha512 --iter-time=10000 --header target/documents/luks-header/header.luks luksFormat /dev/${DEVSYST}
dd if=target/documents/luks-header/header.luks of=/dev/${DEVBOOT}3 status=progress && sync

### Make available the decrypted drive.
# Header in file version :
# sudo cryptsetup luksOpen --header target/boot/luks/header.luks /dev/${DEVSYST} ${DEVSYST}_decrypt
# Header in partition version :
sudo cryptsetup luksOpen --header target/documents/luks-header/header.luks /dev/${DEVSYST} ${DEVSYST}_decrypt

### Prepare disk or partition for LVM use.
sudo pvcreate /dev/mapper/${DEVSYST}_decrypt

### Create LVM volume group.
sudo vgcreate ${VOLUME}--vg /dev/mapper/${DEVSYST}_decrypt

### Create LVM logical volume groups.
sudo lvcreate -L 2G -n swap ${VOLUME}--vg
sudo lvcreate -l 100%FREE -n root ${VOLUME}--vg

### Format logical volume partitions.
sudo mkfs.ext4 -L root /dev/mapper/${VOLUME}----vg-root
sudo mkswap -L swap /dev/mapper/${VOLUME}----vg-swap

### Export Install.md
cp -vf data/static/install/Install.md target/documents/
cat >> target/documents/Install.md << HEREDOC
# <target name> <source device>		<key file>	<options>
${VOLUME}-decrypt /dev/${DEVSYST} none luks,header=/dev/${DEVBOOT}3
HEREDOC
touch target/documents/cryptsetup.command
touch target/documents/crypttab.line
echo "${VOLUME}-decrypt /dev/${DEVSYST} none luks,header=/dev/${DEVBOOT}3" > target/documents/crypttab.line
echo "cryptsetup luksOpen --header /dev/${DEVBOOT}3" /dev/${DEVSYST} ${VOLUME}-crypt > target/documents/cryptsetup.command
# https://wiki.archlinux.org/title/Dm-crypt/Specialties#Encrypted_system_using_a_detached_LUKS_header
