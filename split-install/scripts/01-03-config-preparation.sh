#!/bin/bash 

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

mkdir -pv target/documents/luks-nohead/
tar -xzvf luks-nohead.tar.gz -C target/documents/luks-nohead/
rm -f luks-nohead.tar.gz

sync
umount target
