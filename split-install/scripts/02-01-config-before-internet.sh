#!/bin/bash

# Variables de mise en page

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Configuration de l'interface réseau

echo -e "${BOLD}Configuration de l'anonymisation de l'adresse MAC.${STD}"

# Spoof current mac adresses
sudo ifconfig wlan0 down
sudo ifconfig eth0 down
sudo macchanger -r wlan0
sudo macchanger -r eth0
sudo ifconfig wlan0 up
sudo ifconfig eth0 up

# Enable macchanger at startup
cp data/static/config/macchanger/changemac@.service /etc/systemd/system/
chown root:root /etc/systemd/system/changemac@.service
systemctl enable changemac@wlan0.service
systemctl enable changemac@eth0.service

macchanger -s wlan0
macchanger -s eth0

ip addr 

echo -e "${BOLD}Vous pouvez maintenant vous connecter à internet${STD}"


