#!/bin/bash

# Variables de mise en page

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Element repository	
while true;do
	wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg && break
done	
gpg --import /usr/share/keyrings/element-io-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list

# Signal-desktop repository
while true;do
	wget -O- --directory-prefix=/usr/share/keyrings/ https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > /usr/share/keyrings/signal-desktop-keyring.gpg && break
done
gpg --import /usr/share/keyrings/signal-desktop-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | tee -a /etc/apt/sources.list.d/signal-xenial.list

# Visual studio code
while true;do
wget -qO- --directory-prefix=/usr/share/keyrings/ https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /etc/apt/keyrings/packages.microsoft.gpg && break
done
gpg --import /etc/apt/keyrings/packages.microsoft.gpg
echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list

# SimpleX-chat

# Install packages
apt update
apt install -y apt-transport-https
apt install -y ./mnt/tmp/install/data/dynamic/vpn/*.deb

apt install -y vlc libreoffice code keepassx audacity torbrowser-launcher thunderbird element-desktop signal-desktop tor gimp mat2 macchanger clementine mat2 vlc keepassx macchanger bluetooth rfkill blueman bluez bluez-tools pulseaudio-module-bluetooth 

# Réparation eventuelle d'une installation pétée.

apt install --fix-missing
