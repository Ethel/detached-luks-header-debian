#!/bin/bash

# Variables de mise en page

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Nettoyage
echo
echo -e "${BOLD}Nettoyage...${STD}"
rm -rvf data/dynamic/vpn
rm -rvf data/dynamic/keepassx
apt autoremove -y 
echo

sudo apt autoremove
echo -e "${BOLD}Nettoyage des fichiers d'installation effectué !${STD}"


