#!/bin/bash 

# Variables de mise en page :

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

# Exit on error
# set -e 

### Recuperation des variables
VOLUME=$(cat data/dynamic/variables/VOLUME)
DEVINST=$(cat data/dynamic/variables/DEVINST)
DEVBOOT=$(cat data/dynamic/variables/DEVBOOT)
DEVSYST=$(cat data/dynamic/variables/DEVSYST) 

# Close installation and clean up
swapoff /dev/mapper/${VOLUME}----vg-swap
lvchange -an /dev/${VOLUME}--vg/swap
lvchange -an /dev/${VOLUME}--vg/root
cryptsetup luksClose ${DEVSYST}_decrypt

[[ $? -gt 0 ]] && echo -e "${RED}Démontage des volumes échoué.${STD}"
rm -vrf data/dynamic/variables/*
rm -vrf data/dynamic/packages/*
echo -e "${RED}L'installation est finie, vous pouvez redémarrer sur votre nouveau système.${STD}"
