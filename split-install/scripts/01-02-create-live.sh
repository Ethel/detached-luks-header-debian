#!/bin/bash

# Variables de mise en page

RED='\033[0;31m'
BOLD=$(tput bold)
STD=$(tput sgr0)

### Recuperation des variables
DEVINST=$(cat data/dynamic/variables/DEVINST)

# Get kali last version
KALI=$( curl https://cdimage.kali.org/current/ | grep -G -o kali-linux-202[0-9].[0-9]-live-amd64.iso | head -n1)

echo -e "${BOLD}Si vous possédez déjà une image $KALI, copiez la dans data/dynamic/ puis appuyez sur n'importe quelle touche.${STD}"
read -n 1 -r CONTINUE

if [ -f "data/dynamic/$KALI" ]; 
	then
		echo -e "${BOLD}Kali est déjà présent, pas besoin de le télécharger !${STD}"
	else
		echo -e "${BOLD}Téléchargement de Kali.${STD}"
		while true;do
			wget -T 15 -c --directory-prefix=data/dynamic https://cdimage.kali.org/current/${KALI} && break
		done
		echo -e "${BOLD}Kali a été téléchargé avec succès !${STD}"
fi

echo -e "${BOLD}Recupération du checksum de kali-live pour vérification de l'image.${STD}"

# Remove signatures and sha256sum if already there, then get them and check GPG signature of kali image
if [ -f "data/dynamic/SHA256SUMS.gpg" ]; 
	then
	rm -vf data/dynamic/SHA256SUMS.gpg
fi
if [ -f "data/dynamic/SHA256SUMS" ]; 
	then
	rm -vf data/dynamic/SHA256SUMS
fi

while true;do
	wget -T 15 -c -q -O - https://archive.kali.org/archive-key.asc | gpg --import && break
done

while true;do
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ED444FF07D8D0BF6 && break
done

while true;do
	wget -T 15 -c --directory-prefix=data/dynamic https://cdimage.kali.org/current/SHA256SUMS.gpg && break
done

while true;do
	wget -T 15 -c --directory-prefix=data/dynamic https://cdimage.kali.org/current/SHA256SUMS && break
done

# Check GPG return code and exit if error
cd data/dynamic
gpg --verify SHA256SUMS.gpg SHA256SUMS
cd ../..

[[ $? -gt 0 ]] && echo -e "${RED}Verification échouée, votre image est cassée${STD}" && rm -f data/dynamic/* && exit 1

# Check SHA256SUM of kali image

echo -e "${BOLD}Vérification de l'authenticité de l'image de kali. Cette opération peut prendre un peu de temps.${STD}"

cd data/dynamic
sha256sum -c SHA256SUMS --ignore-missing
cd ../..

# Check SHA256SUM return code and exit if error
[[ $? -gt 0 ]] && echo -e "${RED}Verification echouée${STD}" && rm -f data/dynamic/* && exit 1

echo -e "${BOLD}L'image de Kali semble authentique !${STD}"

echo -e "${BOLD}Création de la clé kali-live${STD}"
dd if=data/dynamic/$KALI of=/dev/${DEVINST} bs=16M status=progress && sync
